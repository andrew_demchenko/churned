# import libraries
import argparse
import pandas as pd
from sklearn import preprocessing
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn.linear_model import LogisticRegression


def preprocess(model):
    # Remove categorical features we are not interested in
    for i in ['housenumber', 'recordstatus', 'contactdescription', 'firstname', 'lastname', 'units']:
        del model[i]
    # Transforms 'null's to NaN
    model['total_resolve_time'] = pd.to_numeric(model['total_resolve_time'], errors='coerce')

    # TODO use sklearn sklearn.preprocessing.Imputer
    for column in ['total_resolve_time', 'hearing_outcomes', 'percent_condo_portfolio', 'percent_coop_portfolio', 'percent_genpart_portfolio',
                   'percent_llc_portfolio', 'percent_corp_portfolio', 'percent_indiv_portfolio',
                   'percent_condominium_portfolio', 'percent_joint_portfolio', 'percent_lp_portfolio',
                   'percent_hdfc_portfolio', 'percent_registered']:
        model[column] = model[column].fillna(0)

    model = model.dropna()
    model = model.drop_duplicates()

    return model


def print_results(result, test_label):
    for index, row in enumerate(result):
        actual_label = test_label.iloc[index, 0]
        verdict = ""
        if row[1] > 0.005:
            if actual_label == 1:
                verdict = "there is a chance that churned=1"
            else:
                verdict = "false positive"
        print "{} {} {:.2f} {:.2f} {}".format(index, actual_label, row[0], row[1], verdict)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', help='Path to your train data in csv format')
    parser.add_argument('--test', help='Path to your test data in csv format')
    parser.add_argument('--output', help='Path to output file')
    args = parser.parse_args()
    train_file = args.train or 'train.csv'
    test_file = args.test or 'test.csv'
    output_file = args.output or 'tree.dot'

    # Read train file
    train = pd.read_csv(train_file, sep=',', low_memory=False)

    # Preprocess train
    train = preprocess(train)

    # Split train
    train_data, train_label = train.iloc[:, 1:], train[['churned']]

    # Read csv test
    test = pd.read_csv(test_file)

    # Preprocess data
    test = preprocess(test)

    # Split data
    test_data, test_label = test.iloc[:, 1:], test[['churned']]

    # Encode categorical features
    categorical_features = ['registered', 'boro', 'streetname', 'zip', 'corporationname']
    categorical_features_indexes = []
    for i in categorical_features:
        le = preprocessing.LabelEncoder()
        le.fit(train_data[i].append(test_data[i]))
        train_data[i] = le.transform(train_data[i])
        test_data[i] = le.transform(test_data[i])
        categorical_features_indexes.append(train_data.columns.get_loc(i))
    ohe = preprocessing.OneHotEncoder(categorical_features=categorical_features_indexes, sparse=True, handle_unknown='ignore')
    ohe.fit(train_data)
    train_data_columns = [x for x in train_data.columns if x not in categorical_features]
    train_data = ohe.transform(train_data)
    test_data = ohe.transform(test_data)
    encoded_data_columns = []
    for (i, feature_name) in enumerate(categorical_features):
        encoded_data_columns += [feature_name] * (ohe.feature_indices_[i + 1] - ohe.feature_indices_[i])
    encoded_data_columns += train_data_columns

    # Normalization
    train_data = preprocessing.normalize(train_data, norm='l2', axis=0)

    # Normalization
    test_data = preprocessing.normalize(test_data, norm='l2', axis=0)

    print "Decision tree"
    tree = DecisionTreeClassifier(criterion='entropy', max_depth=3, random_state=0)
    tree.fit(train_data, train_label)
    export_graphviz(tree, out_file=output_file, feature_names=encoded_data_columns)
    # Optional: innstall GraphViz then run
    # > dot -Tpng tree.dot -o tree.png
    result = tree.predict_proba(test_data)
    print_results(result, test_label)

    print "Logistic regression"
    logistic = LogisticRegression()
    logistic.fit(train_data, train_label)
    result = logistic.predict_proba(test_data)
    print_results(result, test_label)


if __name__ == '__main__':
    main()
