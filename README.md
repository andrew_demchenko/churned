This program analyses "Property Management Data" data set.
It returns predictions for customers' desire to change property management company.

Program inputs:

- Prediction model (default is 'model.csv')
- File to predict (default is 'dataset.csv')

Program output:

- Decision tree (default is 'tree.dot')

To run the program:
```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python main.py
# run python main.py --help to get help
```